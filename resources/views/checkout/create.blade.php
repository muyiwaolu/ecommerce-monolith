@extends('layouts.app')
@section('heading')
<h1>Checkout</h1>
@endsection
@section('content')
<form method="post" action="{{route('checkout.store')}}">
{{csrf_field()}}
<ul>
    @foreach($cart_items as $cart_item)
            <li><a href="{{route('products.show', $cart_item->product->id)}}">{{$cart_item->product->title}}</a> | Amount: <input type="number" min="1" max="{{$cart_item->product->remaining_stock}}" value="1" name="{{$cart_item->product->id}}" id="product-{{$cart_item->product->id}}"  required> | <a href="{{route('cart.delete', $cart_item->product->id)}}">Remove</a> </li>
    @endforeach
    <br>
    <input type="submit" value="Checkout"  class="btn btn-primary">
</ul>
</form>
@endsection
