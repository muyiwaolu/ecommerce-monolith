@extends('layouts.app')
@section('heading')
  <h1>Cart</h1>
@endsection
@section('content')
@if(count($cart_items) > 0)
<ul>
    @foreach($cart_items as $item)
      <li>{{$item->product->title}} - <a href="{{route('cart.delete', $item->id)}}">Remove</a>
    @endforeach
</ul>
<a href="{{route('checkout.create')}}"><p class="btn btn-primary">Checkout</p></a>
@else
<p>There are no items in your cart.</p>
@endif
@endsection
