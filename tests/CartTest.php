<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CartTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @test
     */
    public function cart_item_does_not_add_to_cart_when_item_is_not_in_stock()
    {
        $user = factory(App\User::class)->create();
        $product = factory(App\Product::class)->create(['remaining_stock' => 0]);
        $this->actingAs($user)
             ->visit("/cart/add/{$product->id}")
             ->visit('/cart')
             ->dontSee($product->title)
             ->missingFromDatabase('cart_items', ['product_id' => $product->id]);
    }

}
