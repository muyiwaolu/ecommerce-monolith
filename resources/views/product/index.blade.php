@extends('layouts.app')
@section('heading')
    <h1>Products @if(! Auth::guest() && Auth::user()->is_admin)
<small>(<a href="{{route('products.create')}}">Add a product</a>)?</small>
@endif
</h1>
@endsection
@section('content')
@if(! $products->isEmpty())
    <ul>
        @foreach($products as $product)
            <li><a href="{{route('products.show', $product)}}">{{$product->title}}</a>
        @endforeach
    </ul>
    {{$products->links()}}
@else
<p class="lead">There are no products to display.</p>
@endif

@endsection
