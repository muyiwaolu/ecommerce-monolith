<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Http\Requests;
use App\Product;
use App\CartItem;

class CartController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    private function userCartItems()
    {
        return CartItem::where('user_id', Auth::user()->id)->get();
    }

    public function index(Request $request)
    {
        return response()->json(['cart_items' => $this->userCartItems()]);
    }

    public function store(Request $request, $product_id)
    {
        $existing_in_stock_items = $this->userCartItems()->filter(function($cart_item) use ($product_id) {
            return $cart_item->product->id == $product_id && $cart_item->product->has_stock();
        });
        $product = Product::findOrFail($product_id);
        if ( $existing_in_stock_items->isEmpty() && $product->has_stock() ) {
            CartItem::create(['product_id' => $product_id, 'user_id' => Auth::user()->id]);
        }
        return back();
    }

    public function delete(Request $request, $cart_id)
    {
        $existing_item = CartItem::findOrFail($cart_id);
        $existing_item->delete();
        return back();
    }
}
