<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class HomepageTest extends TestCase
{
    use DatabaseMigrations;
    
    /**
     * @test
     */
    public function home_page_has_correct_content()
    {
        $this->visit('/')
             ->see('total');
    }
}
