<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'title',
        'remaining_stock',
        'options',
        'description',
        'price'
    ];

    public function formatted_price()
    {
        return '£' . $this->price / 100;
    }

    public function has_stock()
    {
        return $this->remaining_stock > 0;
    }
}
