<?php

Route::get('/', 'ProductsController@index');

Route::get('products', 'ProductsController@index')->name('products.index');
Route::get('products/{id}', 'ProductsController@show')->name('products.show');
Route::post('products', 'ProductsController@store');
Route::put('products/{id}', 'ProductsController@update');
Route::delete('products/{id}', 'ProductsController@destroy');

Route::get('cart', 'CartController@index')->name('cart.index');
Route::get('cart/add/{product_id}', 'CartController@store')->name('cart.create');
Route::get('cart/remove/{product_id}', 'CartController@delete')->name('cart.delete');
Route::get('checkout', 'CheckoutController@create')->name('checkout.create');
Route::post('checkout/store', 'CheckoutController@store')->name('checkout.store');
Auth::routes();
