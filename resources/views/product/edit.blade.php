@extends('layouts.app')

@section('heading')
    <h1>Edit {{$product->title}}</h1>
@endsection

@section('content')
    {!! Form::model($product, ['route' => ['products.update', $product->id], 'method' => 'PUT', 'files' => true]) !!}
        <p class="lead">Items marked with an asterisk (*) are required.</p>
        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
            {!! Form::label('title', 'Title *') !!}
            {!! Form::text('title', null, ['class' => 'form-control', 'required' => 'required']) !!}
            <small class="text-danger">{{ $errors->first('title') }}</small>
        </div>

        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
            {!! Form::label('description', 'Description *') !!}
            {!! Form::textarea('description', null, ['class' => 'form-control', 'required' => 'required']) !!}
            <small class="text-danger">{{ $errors->first('description') }}</small>
        </div>
<div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
            {!! Form::label('price', 'Price *') !!}
            {!! Form::number('price', null, ['class' => 'form-control', 'required' => 'required']) !!}
            <small class="text-danger">{{ $errors->first('price') }}</small>
        </div>

        <div class="form-group{{ $errors->has('remaining_stock') ? ' has-error' : '' }}">
            {!! Form::label('remaining_stock', 'Remaining Stock *') !!}
            {!! Form::number('remaining_stock', null, ['class' => 'form-control', 'required' => 'required']) !!}
            <small class="text-danger">{{ $errors->first('remaining_stock') }}</small>
        </div>

        <div class="form-group{{ $errors->has('options') ? ' has-error' : '' }}">
            {!! Form::label('options', 'Options') !!}
            {!! Form::text('options', null, ['class' => 'form-control', 'placeholder' => 'red, blue, green']) !!}
            <small class="text-danger">{{ $errors->first('options') }}</small>
        </div>

        <div class="btn-group">
            {!! Form::reset("Reset", ['class' => 'btn btn-warning']) !!}
            {!! Form::submit("Add", ['class' => 'btn btn-success']) !!}
        </div>

    {!! Form::close() !!}
@endsection
