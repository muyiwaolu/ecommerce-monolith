<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ProductTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @test
     */
    public function index_get_returns_ok_status()
    {
        $this->visit('/products')
             ->assertResponseOk();
    }

    /**
     * @test
     */
    public function test_store_without_middleware_returns_ok_status()
    {
        $this->WithoutMiddleware();

        $response = $this->call('POST', 'products', ['title' => 'test', 'remaining_stock' => 5, 'description' => 'blah', 'price' => 500]);
        $this->assertEquals($response->status(), 302);
        $this->seeInDatabase('products', ['title' => 'test', 'remaining_stock' => 5, 'price' => 500]);
    }

    /**
     * @test
     */
    public function show_get_returns_ok_status_when_product_exists()
    {
        $product = factory(App\Product::class)->create();
        $id = $product->id;
        $this->visit("/products/${id}")
             ->assertResponseOk();
    }

    /**
     * @test
     */
    public function update_put_returns_ok_status_when_product_exists()
    {
        $product = factory(App\Product::class)->create();
        $id = $product->id;
        $new_title = 'Super Amazing Product!';
        $this->WithoutMiddleware();
        $response = $this->call('PUT', "products/${id}", ['title' => $new_title, 'remaining_stock' => 6, 'description' => 'blah', 'price' => 480]);
        $this->assertEquals($response->status(), 302);
        $this->seeInDatabase('products', ['title' => $new_title, 'remaining_stock' => 6, 'price' => 480]);

    }

    /**
     * @test
     */
    public function destroy_delete_returns_ok_status_when_product_exists()
    {
        $product = factory(App\Product::class)->create();
        $id = $product->id;
        $new_title = 'Super Amazing Product!';
        $this->WithoutMiddleware();
        $response = $this->call('DELETE', "products/${id}");
        $this->assertEquals($response->status(), 302);
    }
}
