<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Product;

use Auth;

class CheckoutController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create()
    {
        $cart_items = Auth::user()->cart_items;
        if( $cart_items->isEmpty() ) {
            return back();
        }
        return response()->json($cart_items);
    }

    public function store(Request $request)
    {
        $product_ids = collect($request->except('_token'));
        $products = $product_ids->map(function($amount, $product_id) {
            $product = Product::find($product_id);
            $product->remaining_stock -= $amount;
            $product->save();
            return $product;
        });
        Auth::user()->cart_items()->delete();
        sleep(1);
        return redirect()->route('cart.index');
    }
}
