<?php

$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
        'is_admin' => $faker->boolean,
    ];
});

$factory->define(App\Product::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->words(3, true),
        'description' => $faker->sentence,
        'remaining_stock' => $faker->numberBetween(0, 30),
        'price' => $faker->randomNumber(4)
    ];
});
