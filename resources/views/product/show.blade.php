@extends('layouts.app')
@section('heading')
    <h1>{{$product->title}}</h1>
@endsection
@section('content')
      <p>{{$product->formatted_price()}}</p>
      <p>{{$product->description}}</p>
      <p>Remaining Stock: {{$product->remaining_stock}}</p>
      <hr>
      @if($product->has_stock())
      <p><a href="{{route('cart.create', $product->id)}}">Add to Cart</a> | @endif <a href="{{route('products.index')}}">Back to Index</a></p>
      @if(Auth::check() && Auth::user()->is_admin)
      <p><a href="{{route('products.edit', $product)}}">Edit</a> |
          <a href="{{route('products.destroy', $product)}}">Delete</a></p>
      @endif
@endsection
